import os

def dir_maker():
    if not os.path.exists("../images"):
        os.mkdir("../images")
    folders = ["backgrounds", "labels", "objects", "recombined-images", "white_images"]
    for dir_name in folders:
        if not os.path.exists(f"../images/{dir_name}"):
            os.mkdir(f"../images/{dir_name}")

if __name__ == "__main__":
    dir_maker()
